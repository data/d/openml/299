# OpenML dataset: libras_move

https://www.openml.org/d/299

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Daniel Baptista Dias, Sarajane Marques Peres, Helton Hideraldo Biscaro  
University of Sao Paulo, School of Art, Sciences and Humanities, Sao Paulo, SP, Brazil  
**Source**: Unknown - November 2008  
**Please cite**:   

### LIBRAS Movement Database
LIBRAS, acronym of the Portuguese name "LIngua BRAsileira de Sinais", is the official brazilian sign language. The dataset (movement_libras) contains 15 classes of 24 instances each, where each class references to a hand movement type in LIBRAS. The hand movement is represented as a bidimensional curve performed by the hand in a period of time. The curves were obtained from videos of hand movements, with the Libras performance from 4 different people, during 2 sessions. Each video corresponds to only one hand movement and has about $7$ seconds. Each video corresponds to a function F in a functions space which is the continual version of the input dataset. In the video pre-processing, a time normalization is carried out selecting 45 frames from each video, in according to an uniform distribution. In each frame, the centroid pixels of the segmented objects (the hand) are found, which compose the discrete version of the curve F with 45 points. All curves are normalized in the unitary space.
In order to prepare these movements to be analysed by algorithms, we have carried out a mapping operation, that is, each curve F is mapped in a representation with 90 features, with representing the coordinates of movement. 
Each instance represents 45 points on a bi-dimensional space, which can be plotted in an ordered way (from 1 through 45 as the X coordinate) in order to draw the path of the movement.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/299) of an [OpenML dataset](https://www.openml.org/d/299). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/299/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/299/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/299/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

